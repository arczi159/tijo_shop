package pl.edu.pwsztar;

import pl.edu.pwsztar.entity.Product;
import pl.edu.pwsztar.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    private final ProductRepository productRepository;

    public ShoppingCart() {
        productRepository = new ProductRepository();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if(price <= 0 || quantity <= 0 || productName == null || productName.isEmpty() || ((productRepository.getQuantityOfProducts() + quantity) > ShoppingCartOperation.PRODUCTS_LIMIT)) {
            return false;
        } else {
            Product foundedProduct = productRepository.getProductByName(productName);
            int quantityAdd = 0;
            if(foundedProduct != null) {
                if(foundedProduct.getPrice() != price){
                    return false;
                }
                quantityAdd = foundedProduct.getQuantity();
            }
            Product product = new Product(productName, price, quantity + quantityAdd);
            productRepository.addProduct(product);
            return true;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        Product foundedProduct = productRepository.getProductByName(productName);
        if(quantity <= 0 || foundedProduct == null || foundedProduct.getQuantity() < quantity) {
            return false;
        } else {
            if(foundedProduct.getQuantity() > quantity) {
                Product product = new Product(productName, foundedProduct.getPrice(), foundedProduct.getQuantity() - quantity);
                productRepository.addProduct(product);
            } else {
                productRepository.removeProductByName(productName);
            }
            return true;
        }
    }

    public int getQuantityOfProduct(String productName) {
        Product foundedProduct = productRepository.getProductByName(productName);
        if(foundedProduct == null) {
            return 0;
        } else {
            return foundedProduct.getQuantity();
        }
    }

    public int getSumProductsPrices() {
        return productRepository.getAllProducts()
                .stream()
                .mapToInt(product -> product.getPrice() * product.getQuantity())
                .sum();
    }

    public int getProductPrice(String productName) {
        Product foundedProduct = productRepository.getProductByName(productName);
        if(foundedProduct == null) {
            return 0;
        } else {
            return foundedProduct.getPrice();
        }
    }

    public List<String> getProductsNames() {
        return productRepository.getAllProducts()
                .stream()
                .map(Product::getProductName)
                .collect(Collectors.toList());
    }
}
