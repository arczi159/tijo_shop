package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    void setup() {
        shoppingCart = new ShoppingCart();
    }

    @ParameterizedTest(name = "should add product with name {0} and price {1} and quantity {2} properly")
    @CsvSource({
            "Toilet Paper, 8, 10",
            "Grapes, 15, 8",
            "Strawberry, 21, 6",
            "Chicken, 14, 4",
            "Beer, 4, 10"
    })
    void shouldAddProductsProperly(String productName, int price, int quantity) {
        boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertTrue(result);
    }

    @ParameterizedTest(name = "should not add product with name {0} and price {1} and quantity {2}")
    @CsvSource({
            "Strawberry, -5, 10",
            "Butter, 5, 0",
            "Chicken, 15, -4",
            ", 10, 50"
    })
    void shouldNotAddProductsWithInvalidData(String productName, int price, int quantity) {
        boolean result = shoppingCart.addProducts(productName, price, quantity);
        assertFalse(result);
    }

    @Test
    void shouldDeleteProductsProperly() {
        shoppingCart.addProducts("Milk", 10, 5);
        shoppingCart.addProducts("CocaCola", 15, 20);
        shoppingCart.addProducts("Chicken", 5, 30);

        boolean resultDeleteMilk = shoppingCart.deleteProducts("Milk", 5);
        boolean resultDeleteCocaCola = shoppingCart.deleteProducts("CocaCola", 5);
        boolean resultDeleteChicken = shoppingCart.deleteProducts("Chicken", 1);

        assertTrue(resultDeleteMilk);
        assertTrue(resultDeleteCocaCola);
        assertTrue(resultDeleteChicken);
    }

    @Test
    void shouldNotDeleteProducts() {
        shoppingCart.addProducts("Strawberry", 10, 5);
        shoppingCart.addProducts("Bread", 15, 20);
        shoppingCart.addProducts("CocaCola", 5, 30);
        shoppingCart.addProducts("Apple", 5, 25);

        boolean resultDeleteStrawberry = shoppingCart.deleteProducts("Strawberry", 6);
        boolean resultDeleteBread = shoppingCart.deleteProducts("Bread", -5);
        boolean resultDeleteCocaCola = shoppingCart.deleteProducts("CocaCola", 40);
        boolean resultDeleteApple = shoppingCart.deleteProducts("Apple", 0);

        assertFalse(resultDeleteStrawberry);
        assertFalse(resultDeleteBread);
        assertFalse(resultDeleteCocaCola);
        assertFalse(resultDeleteApple);
    }

    @Test
    void shouldIncreaseProductQuantityWhileAddingTheSameProduct() {
        boolean resultAddMilkFirst = shoppingCart.addProducts("Milk", 100, 1);
        boolean resultAddMilkSecond = shoppingCart.addProducts("Milk", 100, 5);
        boolean resultAddMilkThird = shoppingCart.addProducts("Milk", 100, 15);

        assertTrue(resultAddMilkFirst);
        assertTrue(resultAddMilkSecond);
        assertTrue(resultAddMilkThird);
    }

    @Test
    void shouldNotIncreaseProductQuantityWhileChangingPrice() {
        boolean resultAddStrawberryFirst = shoppingCart.addProducts("Strawberry", 100, 1);
        boolean resultAddStrawberrySecond = shoppingCart.addProducts("Strawberry", 101, 5);
        boolean resultAddStrawberryThird = shoppingCart.addProducts("Strawberry", 100, 15);

        assertTrue(resultAddStrawberryFirst);
        assertFalse(resultAddStrawberrySecond);
        assertTrue(resultAddStrawberryThird);
    }

    @Test
    void shouldNotAddProductIfProductsLimitHasBeenReached() {
        boolean resultAddStrawberry = shoppingCart.addProducts("Strawberry", 5, 1);
        boolean resultAddBeerFirst = shoppingCart.addProducts("Beer", 100, 498);
        boolean resultAddBeerSecond = shoppingCart.addProducts("Beer", 100, 1);
        boolean resultAddBeerThird = shoppingCart.addProducts("Beer", 100, 1);

        assertTrue(resultAddStrawberry);
        assertTrue(resultAddBeerFirst);
        assertTrue(resultAddBeerSecond);
        assertFalse(resultAddBeerThird);
    }

    @Test
    void shouldGetQuantityOfProduct() {
        shoppingCart.addProducts("Grapes", 1,2);
        shoppingCart.addProducts("CocaCola", 5, 10);

        int resultGrapes = shoppingCart.getQuantityOfProduct("Grapes");
        int resultCocaCola = shoppingCart.getQuantityOfProduct("CocaCola");
        int resultOfNotExistingProduct = shoppingCart.getQuantityOfProduct("Strawberry");

        assertEquals(2, resultGrapes);
        assertEquals(10, resultCocaCola);
        assertEquals(0, resultOfNotExistingProduct);
    }

    @Test
    void shouldGetSumProductsPrices() {
        shoppingCart.addProducts("Milk", 2, 15);
        shoppingCart.addProducts("Strawberry", 2, 21);
        shoppingCart.addProducts("CocaCola", 20, 5);

        int sumProductsPrices = shoppingCart.getSumProductsPrices();

        assertEquals(172, sumProductsPrices);
    }

    @Test
    void shouldGetSumProductsPricesEqualsToZeroIfShoppingCartIsEmpty() {
        int sumProductsPrices = shoppingCart.getSumProductsPrices();

        assertEquals(0, sumProductsPrices);
    }

    @Test
    void shouldGetProductPrice() {
        shoppingCart.addProducts("CocaCola", 20, 5);
        shoppingCart.deleteProducts("CocaCola", 2);
        int cocaColaPrice = shoppingCart.getProductPrice("CocaCola");

        assertEquals(20, cocaColaPrice);
    }

    @Test
    void shouldGetProductNames() {
        shoppingCart.addProducts("CocaCola", 20, 1);
        shoppingCart.addProducts("Butter", 15, 12);
        shoppingCart.addProducts("Milk", 10, 21);
        shoppingCart.addProducts("Strawberry", 20, 3);

        List<String> result = shoppingCart.getProductsNames();
        List<String> names = Arrays.asList("CocaCola", "Butter", "Milk","Strawberry");

        assertTrue(names.containsAll(result));
    }

    @Test
    void shouldNotGetProductNamesIfShoppingCartIsEmpty() {
        List<String> result = shoppingCart.getProductsNames();

        assertTrue(result.isEmpty());
        assertEquals(0, result.size());
    }

}
